<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_translation extends Model {

    
    protected $fillable = [
        'id',
        'name',
        'description',
    ];
    protected $hidden = [
        'pivot'
    ];

    public function categories() {
        return $this->belongsTo(Category::class);
    }

}
